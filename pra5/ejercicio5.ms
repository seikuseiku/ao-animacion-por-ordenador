-- Distancia entre dos puntos
Function distancia P1 P2 =(
	return sqrt((P1.x-P2.x)^2 + (P1.y-P2.y)^2 + (P1.z-P2.z)^2)
)

-- Punto medio
Function PuntoMedio P1 P2 =(
	X = (P1.x + P2.x)/2
	Y = (P1.y + P2.y)/2
	Z = (P1.z + P2.z)/2 + (distancia P1 P2)/2 -- subida segun la distancia entre huellas / 2
	return [X,Y,Z]
)

-- Interpolacion lineal
Function interpolacion_lineal P1 P2 inicio final t = (
	VectorSalto = P2-P1
	return P1 + ((normalize VectorSalto)*(((length VectorSalto)/(final-inicio))*(t-inicio)))
)

-- Interpolacion cuadratica
Function interpolacion_cuadratica P1 P2 P3 inicio final t  = (
	lineal1 = interpolacion_lineal P1 P2 inicio final t 
	lineal2 = interpolacion_lineal P2 P3 inicio final t 
	return (interpolacion_lineal lineal1 lineal2 inicio final t)
)

-- Animar
Function Animar Traveler inicio final pausa = (
	intervalo = (final-inicio)/(huellas.count-1)
	
	escala_original = Traveler.scale	-- valores de escalado
	escala_reposo = 	[escala_original.x, escala_original.y, 1.0]	
	escala_impulso = [escala_original.x, escala_original.y, 1.6]	
	escala_arriba = 	[0.7, 0.7, 1.6]										
	
	animate on(		
		-- posicion inicial
		at time inicio (
			Traveler.pos = huellas[1].pos
			Traveler.scale = escala_original
		)
		
		-- por cada par de huellas consecutivas en el array huellas
		for i in 1 to huellas.count-1 do (
			marca = inicio + intervalo*(i-1)
			punto_medio = PuntoMedio huellas[i].pos huellas[i+1].pos
			
			-- rotacion y escalado
			at time (marca+pausa) (						-- salta
				Traveler.scale = escala_impulso
			)
			at time (marca+(intervalo/2)) (			-- a mitad de recorrido
				Traveler.scale = escala_arriba
			)
			at time (marca+intervalo-pausa) ( 		-- llega
				Traveler.scale = escala_impulso
			)
			at time (marca+intervalo) ( 					-- reposo
				Traveler.scale = escala_reposo
			)
			
			-- interpolacion
			for t in (marca+pausa) to (marca+intervalo-pausa) by 1 do(				
				at time t(
					Traveler.pos = interpolacion_cuadratica huellas[i].pos punto_medio huellas[i+1].pos (marca+pausa) (marca+intervalo-pausa) t 
				)
			)
		)
	)
)


-- Dialogo
rollout Salto "Salto"(
	label EActor "Actor:" 
	pickbutton UI_SelectorActor "Selecciona Actor"
	
	spinner UI_IF "Comienzo" range:[0,1000,0] type:#Integer
	spinner UI_FF "Duracion" range:[5,100,100] type:#Integer
	spinner UI_Pausa "Pausa" range:[0,5,3] type:#Integer
	button UI_CrearSalto "Crear animación"
	button UI_Animar "Reproducir"
	button UI_Parar "Parar"
	
	on UI_SelectorActor picked obj do(
		global Traveler = obj
		UI_SelectorActor.text = obj.name
	)
	
	on UI_CrearSalto pressed do(
		if Traveler!= undefined do(
			-- crear array de huellas
			global huellas = $huella* as Array
			deletekeys Traveler
			inicio = UI_IF.value
			final  = UI_IF.value+UI_FF.value
			pausa = UI_Pausa.value
			
   	        Animar Traveler inicio final pausa
			Traveler.showtrajectory = on
		)
    )
	
	on UI_Animar pressed do(
		playAnimation()
	)
	
	on UI_Parar pressed do(
		stopAnimation()
	)
)

createDialog Salto