-- Crear 5 cubos
Cubo1 = Box pos:[-50,50,0] isSelected:on width:8 length:8 height:8 wireColor:blue
Cubo2 = Box pos:[-50,25,0] isSelected:on width:8 length:8 height:8 wireColor:red
Cubo3 = Box pos:[-50,0,0] isSelected:on width:8 length:8 height:8 wireColor:green	
Cubo4 = Box pos:[-50,-25,0] isSelected:on width:8 length:8 height:8 wireColor:yellow
Cubo5 = Box pos:[-50,-50,0] isSelected:on width:8 length:8 height:8 wireColor:orange

-- Animar los cubos
animate on( 
	-- Cubo 1
	at time 0 Cubo1.pos = [-50,50,0]
	at time 100 Cubo1.pos = [50,50,0]
	-- Cubo 2
	at time 20 Cubo2.pos = [-50,25,0]
	at time 40 Cubo2.pos = [50,25,0]
	-- Cubo 3
	at time 20 Cubo3.pos = [-50,0,0]
	at time 60 Cubo3.pos = [50,0,0]
	-- Cubo 4
	at time 60 Cubo4.pos = [-50,-25,0]
	at time 80 Cubo4.pos = [50,-25,0]
	-- Cubo 5
	at time 20 Cubo5.pos = [-50,-50,0]
	at time 100 Cubo5.pos = [50,-50,0]
)
