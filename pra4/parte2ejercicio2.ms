-- Crear tres esferas
ball1 = sphere radius:8 wireColor:orange
ball2 = sphere radius:8 wireColor:orange
ball3 = sphere radius:8 wireColor:orange
ball4 = sphere radius:5 wireColor:blue
-- Posiciones de las esferas
ball1.pos = [-50,50,0]
ball2.pos = [50,50,0]
ball3.pos = [50,-50,0]
ball4.pos = [0,0,0]

animate on(  
	at time 0 ball1.pos = [-50,50,0]
	at time 0 ball2.pos = [50,50,0]
	at time 0 ball3.pos = [50,-50,0]	
	at time 0 ball4.pos = [0,0,0]
	at time 100 ball1.pos = [-50,-50,0]
	at time 100 ball2.pos = [-35,50,0]
	at time 100 ball3.pos = [50,50,0]
)

-- Pesos con los que se va a hacer la restriccion de posicionamiento
peso1 = 0.33;
peso2 = 0.33;
peso3 = 0.33;

animate on for t in 0 to 100 by 5 do
	at time t(
		ball4.pos = (ball1.pos * peso1) + (ball2.pos * peso2) + (ball3.pos * peso3)
	)
