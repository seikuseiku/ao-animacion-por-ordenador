-- Funcion de la interpolacion lineal
fn interpolacion_lineal p1 p2 t maximo =(
	V = (p2 - p1)
	return (p1 + ((normalize V)*(((length V)/maximo*t))))
)

-- Funcion de la interpolacion cuadratica
fn interpolacion_cuadratica p1 p2 p3 t maximo =(
	-- primero hago las lineales por separado
	lineal1 = interpolacion_lineal p1 p2 t maximo
	lineal2 = interpolacion_lineal p2 p3 t maximo
	-- y las junto para calcular la cuadratica
	return (interpolacion_lineal lineal1 lineal2 t maximo)
)

-- Creacion de los objetos
Caja1 = Box pos:[-50,50,0] width:5 length:5 height:5 wireColor:orange
Caja2 = Box pos:[50,50,0]  width:5 length:5 height:5 wireColor:orange
Bola1 = Sphere radius:2 pos:[-45,50,2] segs:32 wireColor:yellow
Bola2 = Sphere radius:4 pos:[0,0,0] segs:32 wireColor:blue

-- Animar los objetos
animate on (
	for t in 0 to 100 by 1 do(
		at time t (
			at time 0 Bola1.pos = [-45,-50,2]
			at time 100 Bola1.pos = [45,-50,2]
			Bola2.pos = interpolacion_cuadratica Caja1.pos Bola1.pos Caja2.pos t 100
		)
	)
)