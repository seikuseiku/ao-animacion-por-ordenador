-- Funcion para actualizar la posici�n 
function Posicionar Guia Copia Superficie Inicio Fin =(
	animate on(
		for t in Inicio to Fin do(
			at time t(
				-- Igualamos la posici�n del objeto copia a la del objeto gu�a
				Copia.pos.x = Guia.pos.x
				Copia.pos.y = Guia.pos.y
				
				-- Para calcular la posici�n en el eje z del objeto copia respecto a la superficie creamos un rayo desde el objeto gu�a a la superficie
				-- Si el plano est� en horizontal
				--Rayo = ray Guia.pos [0,0,-1]
				-- Si el plano est� en vertical (hacia el eje x en negativo)
				Rayo = ray Guia.pos [-1,0,0]
				
				-- Calculamos d�nde colisiona el rayo con la superficie
				global Colision = IntersectRay Superficie Rayo
				
				-- Desplazamos en el eje Z la esfera para que no se fusione con la superficie (la desplazamos lo que vale el radio de la esfera en el vector normal de la colisi�n)
				-- Como buscamos que est� en el otro lado de la cara, le desplazamos el radio pero en el sentido contrario.
				Copia.pos = ((normalize Colision.dir) * -Copia.radius  ) + Colision.pos
			)
		)
	)
)

-- Funcion para actualizar la rotaci�n
function Rotar Objeto Inicio Fin =(	
	animate on (
		for t in Inicio to Fin do(
			at time t(
				-- Obtenemos el vector director restando la posici�n del objeto en t con la posici�n en t-1 (actual - anterior)
				vector_dir = Objeto.pos - (at time (t-1) (Objeto.pos))
					
				-- Obtenemos el vector rotaci�n como el producto vectorial entre el vector director (en x,y) y el vector colision (en z)
				-- Hacemos esto porque el vector rotaci�n es perpendicular al vector director, y el producto vectorial da un vector perpendicular a los dos vectores a los que se les hace el producto.
				vector_rot = cross vector_dir Colision.dir
				
				-- Calculamos el �ngulo de rotaci�n del objeto
				angulo_rot = ((length vector_dir)/(Objeto.radius*pi))*180
				
				-- Creamos el cuaternio con el �ngulo y el vector de rotaci�n
				cuaternio = quat -angulo_rot (normalize vector_rot)
				
				-- Para saber cu�nto hay que rotar en los ejes x, y, z convertimos el cuaternio en una rotaci�n de Euler
				euler_rot = QuatToEuler cuaternio
				
				-- Finalmente rotamos el objeto seg�n la rotaci�n de Euler
				rotate Objeto euler_rot
			)
		)
	)
)

-- Crear la ventana para introducir los par�metros de la animaci�n
rollout ventana "Pr�ctica 6"(
	group "Objetos" (
		label labelGuia "Objeto gu�a:"
		button buttonGuia "Seleccionar"
		label labelCopia "Objeto copia:"
		button buttonCopia "Seleccionar"
		label labelSuperficie "Objeto superficie:"
		button buttonSuperficie "Seleccionar"
	)
	
	group "Duraci�n"(
		spinner UI_InicioAni "Inicio: " range:[0,1000,0] type:#Integer
		spinner UI_FinalAni "Final: " range:[1,1000,100] type:#Integer
	)

	button UI_CrearAnimacion "Crear animaci�n"
	
	-- Seleccionar objeto guia
	on buttonGuia pressed do(
		objectsSelected =getCurrentSelection()
		if (objectsSelected.count == 0) then (
			buttonGuia.text = "Seleccionar"
		) 
		else (
			global Guia = objectsSelected[1]
			buttonGuia.text = Guia.name
		)
	)
	
	-- Seleccionar objeto copia
	on buttonCopia pressed do(
		objectsSelected =getCurrentSelection()
		if (objectsSelected.count == 0) then (
			buttonCopia.text = "Seleccionar"
		) 
		else (
			global Copia = objectsSelected[1]
			buttonCopia.text = Copia.name
		)
	)
	
	-- Seleccionar objeto Superficie
	on buttonSuperficie pressed do(
		objectsSelected =getCurrentSelection()
		if (objectsSelected.count == 0) then (
			buttonSuperficie.text = "Seleccionar"
		) 
		else (
			global Superficie = objectsSelected[1]
			buttonSuperficie.text = Superficie.name
		)
	)

	-- Crear la animaci�n
	on UI_CrearAnimacion pressed do(
		if ((Guia!= undefined) AND (Copia != undefined) AND (Superficie !=undefined)) do(
			deletekeys Copia
			inicio = UI_InicioAni.value
			fin = UI_FinalAni.value
			
			Posicionar Guia Copia Superficie inicio fin
			Rotar Copia (inicio+1) fin
		)
    )
)

createDialog ventana